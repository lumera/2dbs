package br.com.lumera.web;

import br.com.lumera.entity.registro.TbUsuarioDbRegistro;
import br.com.lumera.repositories.registro.RegistroRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by fabioebner on 11/03/16.
 */
@RestController
@RequestMapping("/registro")
public class UsuarioRegistro {

    @Autowired
    RegistroRepo usuarioRegistroRep;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<TbUsuarioDbRegistro> testar(){
        return usuarioRegistroRep.findAll();
    }
}
