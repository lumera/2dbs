package br.com.lumera.web;

import br.com.lumera.entity.dnaso.TbUsuarioDbDnaso;
import br.com.lumera.repositories.dnaso.DnasoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by fabioebner on 11/03/16.
 */
@RestController
@RequestMapping("/dnaso")
public class UsuarioDnaso {

    @Autowired
    DnasoRepo UsuarioDnaRepo;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<TbUsuarioDbDnaso> testar(){
        return (List<TbUsuarioDbDnaso>) UsuarioDnaRepo.findAll();
    }
}
