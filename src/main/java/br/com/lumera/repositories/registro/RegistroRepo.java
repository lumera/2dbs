package br.com.lumera.repositories.registro;

import br.com.lumera.entity.registro.TbUsuarioDbRegistro;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by fabioebner on 17/03/16.
 */
public interface RegistroRepo extends JpaRepository<TbUsuarioDbRegistro, Integer>{
}
