package br.com.lumera.repositories.dnaso;

import br.com.lumera.entity.dnaso.TbUsuarioDbDnaso;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by fabioebner on 17/03/16.
 */
public interface DnasoRepo extends JpaRepository<TbUsuarioDbDnaso, Integer>{
}
