package br.com.lumera.config;

import br.com.lumera.entity.dnaso.TbUsuarioDbDnaso;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", basePackages = {"br.com.lumera.repositories.dnaso"})
@EnableTransactionManagement
public class DbDnasoConfig {
    @Bean(name = "dataSource")
    @Primary
    @ConfigurationProperties(prefix = "datasource.dnaso")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "entityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean dbdnasoEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("dataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("br.com.lumera.entity.dnaso")
                .persistenceUnit("dbdnaso")
                .build();
    }
    @Bean(name = "transactionManager")
    @Primary
    public JpaTransactionManager dbRegistroTransactionManager(@Qualifier("entityManagerFactory") final EntityManagerFactory factory)
    {
        return new JpaTransactionManager(factory);
    }
}
