package br.com.lumera.config;


import br.com.lumera.entity.registro.TbUsuarioDbRegistro;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "dbregistroEntityManagerFactory",basePackages = {"br.com.lumera.repositories.registro"})
@EnableTransactionManagement
public class DbRegistroConfig {

    @Bean(name = "dbRegistroDataSource")
    @ConfigurationProperties(prefix = "datasource.dbregistro")
    public DataSource secondaryDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dbregistroEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean dbregistroEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("dbRegistroDataSource") DataSource dbRegistroDataSource) {
        return builder
                .dataSource(dbRegistroDataSource)
                .packages("br.com.lumera.entity.registro")
                .persistenceUnit("dbregistro")
                .build();
    }
    @Bean(name = "dbregistroTransactionManager")
    public JpaTransactionManager dbRegistroTransactionManager(@Qualifier("dbregistroEntityManagerFactory") final EntityManagerFactory factory)
    {
        return new JpaTransactionManager(factory);
    }
}
