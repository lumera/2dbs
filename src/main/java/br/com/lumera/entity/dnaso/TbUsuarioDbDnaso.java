package br.com.lumera.entity.dnaso;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by fabioebner on 04/01/16.
 */
@Entity
@Table(name = "tb_usuario", catalog="portalservicos", schema="db_portalservicos")
@SequenceGenerator(sequenceName="db_portalservicos.tb_usuario_seq",allocationSize=1, initialValue=0, name = "db_portalservicos.tb_usuario_seq")
public class TbUsuarioDbDnaso implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY, generator="db_portalservicos.tb_usuario_seq")
    @Column(name = "cd_usuario")
    private Integer cdUsuario;


    @Column(name="nm_usuario")
    @NotEmpty
    @Length(max = 250)
    private String nmUsuario;


    @Column(name="nm_email", unique= true)
    @NotEmpty
    @Length(max = 250)
    private String nmEmail;

    @Column(name="nr_cpf")
    @NotEmpty
    @Length(max = 250)
    private String nrCpf;

    @Column(name="ds_senha")
    @NotEmpty
    @Length(max = 250)
    private String dsSenha;

    @Column(name="ds_lembrete_senha")
    @NotEmpty
    @Length(max = 250)
    private String dsLembreteSenha;

    @Column(name="nr_nivel_acesso")
    @NotNull
    private Long nrNivelAcesso;

    @Column(name="nr_cep")
    @Length(max = 250)
    private String nrCep;

    @Column(name="nm_tipo_logradouro")
    @Length(max = 250)
    private String nmTipoLogradouro;

    @Column(name="nm_logradouro")
    @Length(max = 250)
    private String nmLogradouro;

    @Column(name="nr_logradouro")
    @Length(max = 250)
    private String nrLogradouro;

    @Column(name="nm_bairro")
    @Length(max = 250)
    private String nmBairro;

    @Column(name="nm_complemento_logradoudo")
    @Length(max = 250)
    private String nmComplementoLogradouro;

    @Column(name="nm_cidade")
    @Length(max = 250)
    private String nmCidade;

    @Column(name="sg_estado")
    @Length(max = 250)
    private String sgEstado;

    @Column(name="nr_telefone_contato")
    @Length(max = 250)
    private String nrTelefoneContato;

    @Column(name = "vl_credito")
    private BigDecimal vlCredito;

    public BigDecimal getVlCredito() {
        return vlCredito;
    }

    public void setVlCredito(BigDecimal vlCredito) {
        this.vlCredito = vlCredito;
    }

    public Integer getCdUsuario() {
        return cdUsuario;
    }

    public void setCdUsuario(Integer cdUsuario) {
        this.cdUsuario = cdUsuario;
    }

    public String getNmUsuario() {
        return nmUsuario;
    }

    public void setNmUsuario(String nmUsuario) {
        this.nmUsuario = nmUsuario;
    }

    public String getNmEmail() {
        return nmEmail;
    }

    public void setNmEmail(String nmEmail) {
        this.nmEmail = nmEmail;
    }

    public String getNrCpf() {
        return nrCpf;
    }

    public void setNrCpf(String nrCpf) {
        this.nrCpf = nrCpf;
    }

    public String getDsSenha() {
        return dsSenha;
    }

    public void setDsSenha(String dsSenha) {
        this.dsSenha = dsSenha;
    }

    public String getDsLembreteSenha() {
        return dsLembreteSenha;
    }

    public void setDsLembreteSenha(String dsLembreteSenha) {
        this.dsLembreteSenha = dsLembreteSenha;
    }

    public Long getNrNivelAcesso() {
        return nrNivelAcesso;
    }

    public void setNrNivelAcesso(Long nrNivelAcesso) {
        this.nrNivelAcesso = nrNivelAcesso;
    }

    public String getNrCep() {
        return nrCep;
    }

    public void setNrCep(String nrCep) {
        this.nrCep = nrCep;
    }

    public String getNmTipoLogradouro() {
        return nmTipoLogradouro;
    }

    public void setNmTipoLogradouro(String nmTipoLogradouro) {
        this.nmTipoLogradouro = nmTipoLogradouro;
    }

    public String getNmLogradouro() {
        return nmLogradouro;
    }

    public void setNmLogradouro(String nmLogradouro) {
        this.nmLogradouro = nmLogradouro;
    }

    public String getNrLogradouro() {
        return nrLogradouro;
    }

    public void setNrLogradouro(String nrLogradouro) {
        this.nrLogradouro = nrLogradouro;
    }

    public String getNmComplementoLogradouro() {
        return nmComplementoLogradouro;
    }

    public void setNmComplementoLogradouro(String nmComplementoLogradouro) {
        this.nmComplementoLogradouro = nmComplementoLogradouro;
    }

    public String getNmCidade() {
        return nmCidade;
    }

    public void setNmCidade(String nmCidade) {
        this.nmCidade = nmCidade;
    }

    public String getSgEstado() {
        return sgEstado;
    }

    public void setSgEstado(String sgEstado) {
        this.sgEstado = sgEstado;
    }

    public String getNrTelefoneContato() {
        return nrTelefoneContato;
    }

    public void setNrTelefoneContato(String nrTelefoneContato) {
        this.nrTelefoneContato = nrTelefoneContato;
    }


    public void setNmBairro(String nmBairro) {
        this.nmBairro = nmBairro;
    }

    public String getNmBairro() {
        return nmBairro;
    }
}