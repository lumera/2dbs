package br.com.lumera.entity.registro;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by fabioebner on 04/01/16.
 */
@Entity
@Table(name = "tb_usuario_site", catalog="db_registro", schema="public")
//@SequenceGenerator(sequenceName="db_portalservicos.tb_usuario_seq",allocationSize=1, initialValue=0, name = "db_portalservicos.tb_usuario_seq")
public class TbUsuarioDbRegistro implements Serializable {
    @Id
//    @GeneratedValue(strategy=GenerationType.IDENTITY, generator="db_portalservicos.tb_usuario_seq")
    @Column(name = "cd_usuario")
    private Integer cdUsuario;


    @Column(name="nm_cliente")
    @NotEmpty
    @Length(max = 250)
    private String nmUsuario;


    public Integer getCdUsuario() {
        return cdUsuario;
    }

    public void setCdUsuario(Integer cdUsuario) {
        this.cdUsuario = cdUsuario;
    }
}